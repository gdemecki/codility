package com.gdemecki.codility;

import static org.fest.assertions.api.Assertions.assertThat;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PermMissingElemTest {

    private final PermMissingElem instance = new PermMissingElem();

    @DataProvider
    Object[][] validTestCases() {
        return new Object[][] {
                { new int[] { 2, 3, 1, 5 }, 4 },
                { new int[] { 1, 2, 4 }, 3 },
                { new int[] { 5, 4, 3, 1 }, 2 }
        };
    }

    @Test(dataProvider = "validTestCases")
    public void shouldDivisiblesBeCalculatedCorrectly(int[] A, int expectedOutput) {
        assertThat(instance.solution(A))
                .isEqualTo(expectedOutput);
    }

}
