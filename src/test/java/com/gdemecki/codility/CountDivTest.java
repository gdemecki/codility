package com.gdemecki.codility;

import static org.fest.assertions.api.Assertions.assertThat;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CountDivTest {

    private CountDiv instance;

    @BeforeClass
    void prepareClassUnderTest() {
        instance = new CountDiv();
    }

    @DataProvider
    Object[][] validTestCases() {
        return new Object[][] {
                { 6, 11, 2, 3}, // 6, 8 ,10
                { 1, 11, 1, 11},
                { 5, 5, 5, 1}, // A = B
                { 5, 5, 7, 0},
                { 0, 0, 300, 1},
                { 0, 7, 3, 3}, // 0, 3, 6
                { 1, 8, 3, 2}, // 3, 6
                { 2, 4, 2, 2},
                {11, 345, 17, 20}, // prime numbers are special cases!
                {11, 35, 17, 2} // prime numbers are special cases!
        };
    }

    @Test(dataProvider = "validTestCases")
    public void shouldDivisiblesBeCalculatedCorrectly(int A, int B, int K, int expectedOutput) {
        assertThat(instance.solution(A, B, K))
                .isEqualTo(expectedOutput);
    }
}
