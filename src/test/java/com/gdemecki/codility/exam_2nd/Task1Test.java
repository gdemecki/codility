package com.gdemecki.codility.exam_2nd;

import static org.fest.assertions.api.Assertions.assertThat;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class Task1Test {

    Task1 instance = new Task1();

    @DataProvider
    Object[][] validTestCases() {
        return new Object[][] {
                { new int[] { 7, 3, 7, 3, 1, 3, 4, 1 }, 5 },
                { new int[] { 7, 3, 3, 3, 3, 7, 1, 3, 4, 1 }, 4 },
                { new int[] { 7, 3, 4, 3, 4, 7, 0 }, 4 },
                { new int[] { 1, 1, 1, 2 }, 2 },
                { new int[] { 7, 0, 0, 0, 3 }, 5 },
                { new int[] { 7, 0, 0, 0 }, 2 },
                { new int[] { 2, 2, 2, 2 }, 1 },
                { new int[] { 2, 0, 2, 0 }, 2 },
                { new int[] { 1 }, 1 },
                { new int[] {}, 0 },
                { new int[] { 1, 2, 3, 4, 5 }, 5 },
                { new int[] { 1, 2, 1, 3, 4 }, 4 },
                { new int[] { 1, 2, 1, 3, 2 }, 3 }
        };
    }

    @Test(dataProvider = "validTestCases")
    public void shouldWork(int[] A, int expectedOutput) {
        assertThat(instance.solution(A)).isEqualTo(expectedOutput);
    }

}
