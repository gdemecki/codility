package com.gdemecki.codility.exam_2nd;

import static org.fest.assertions.api.Assertions.assertThat;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Task2Test {

    Task2 instance = new Task2();

    @DataProvider
    Object[][] validTestCases() {
        return new Object[][] {
                { new int[] { 4, 2, 2, 3, 1, 4, 7, 8, 6, 9 }, new int [] {5, 9} },
                { new int[] { 1, 2, 4 }, new int [] {0, 1, 2 }},
                { new int[] { 10, 9, 8 }, new int [] {}},
                { new int[] { 1, 1, 1 }, new int [] {0, 1, 2}},
                { new int[] {}, new int[] {} },
                { new int[] { 2, 2, 8, 1, 9 }, new int[] { 4 } }

        };
    }

    @Test(dataProvider = "validTestCases")
    public void shouldWork(int[] A, int[] possibleSolutions) {
        int solution = instance.solution(A);
        System.out.println("bla " + solution);

        if (possibleSolutions.length == 0) {
            assertThat(solution).isEqualTo(-1);
        } else {
            assertThat(possibleSolutions).contains(solution);
        }
    }

}
