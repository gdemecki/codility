package com.gdemecki.codility.exam_2nd;

import static org.fest.assertions.api.Assertions.assertThat;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Task3TriangleTest {

    Task3Triangle instance = new Task3Triangle();

    @DataProvider
    Object[][] validTestCases() {
        return new Object[][] {
                { new int[] { 10, 2, 5, 1, 8, 20 }, 23},
                { new int[] { 5, 10, 18, 7, 8, 3 }, 15},
                { new int[] { 5, 10, 14 }, 29},
                { new int[] { 5, 10, 15 }, -1},
                { new int[] {}, -1},
                { new int[] { 5 }, -1},
                { new int[] { 5, 10 }, -1},
                { new int[] { 10, 20, 30}, -1}
        };
    }

    @Test(dataProvider = "validTestCases")
    public void shouldWork(int[] A, int expectedOutput) {
        assertThat(instance.solution(A)).isEqualTo(expectedOutput);
    }

}
