package com.gdemecki.codility;

import static org.fest.assertions.api.Assertions.assertThat;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TreeHeightTest {

    TreeHeight instance = new TreeHeight();

    @DataProvider
    Object[][] validTestCases() {
        return new Object[][] {
                { createSimpleTree(), 2},
                { createTreeWithOnlyRoot(), 0},
        };
    }

    @Test(dataProvider = "validTestCases")
    public void shouldCalculateCorrectly(Tree tree, int expectedOutput) {
        assertThat(instance.solution(tree))
                .isEqualTo(expectedOutput);
    }

    private Tree createSimpleTree() {
        final Tree root = new Tree(5);
        root.l = new Tree(3);
        root.r = new Tree(10);

        root.l.l = new Tree(20);
        root.l.r = new Tree(21);

        root.r.l = new Tree(1);

        return root;
    }

    private Tree createTreeWithOnlyRoot() {
        return new Tree(1);
    }

}
