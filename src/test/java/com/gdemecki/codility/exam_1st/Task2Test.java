package com.gdemecki.codility.exam_1st;

import static org.fest.assertions.api.Assertions.assertThat;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Task2Test {

    private final Task2 task2 = new Task2();

    @DataProvider
    Object[][] validTestCases() {
        return new Object[][] {
                { new int[] { 1, 3, -3 }, 6 }, // because: A[1] + A[1] + (1 - 1) = 3 + 3 + 0 = 6
                { new int[] { -8, 4, 0, 5, -3, 6 }, 14 }, // because: A[5] + A[1] + (5 - 1) = 6 + 4 + 4 = 14
                { new int[] { -20, 1, -20, -100, 2, -100, 1}, 7 },
                { new int[] { 0, 0, 4, 8, 0}, 16 },
                { new int[] { 0, 0, 4, 0, 5, 0}, 11 },
                { new int[] { 0, 2, 0, 0, 0, 0, 0}, 7 },
                { new int[] { 1, 1, 1, 1, 2, 1, 1, 1, 1, 1 }, 11 },
                { new int[] { 1, 1, 1, 1, 2, 1, 1, 1, 3, 1 }, 12 },
                { new int[] { 1, 1, 1, 1, 2, 2, 2, 2, 3, 0, 0, 0, 0 }, 13 },
                { new int[] { 1, 1, 1, 1, 90_000, 1, 1, 1, 1}, 180_000 },
                { new int[] { -1, 20 }, 40 },
                { new int[] { 0, 0, 0 }, 2 },
                { new int[] { 1, 1, 1 }, 4 }
        };
    }

    @Test(dataProvider = "validTestCases")
    public void shouldWork(int[] A, int expectedOutput) {
        assertThat(task2.solution(A))
                .isEqualTo(expectedOutput);
    }

}
