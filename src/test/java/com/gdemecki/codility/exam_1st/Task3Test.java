package com.gdemecki.codility.exam_1st;

import static org.fest.assertions.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Task3Test {

    private final Task3 instance = new Task3();

    @DataProvider
    Object[][] validTestCases() {
        return new Object[][] {
                { 11, Arrays.asList(new int[] { 1, 3, 7 }) },
                { 8, Arrays.asList(new int[] { 1, 7 }, new int[] { 3, 5 }) },
                { 1, Arrays.asList(new int[] { 1 }) },
                { 2, Collections.emptyList() },
                { 3, Arrays.asList(new int[] { 3 }) },
                { 4, Arrays.asList(new int[] { 1, 3 }) },
                { 5, Arrays.asList(new int[] { 5 }) }
        };
    }

    @Test(dataProvider = "validTestCases")
    public void shouldDivisiblesBeCalculatedCorrectly(int M, List<int[]> possibleSolutions) {
        int[] solution = instance.solution(M);

        if (possibleSolutions.isEmpty()) {
            assertThat(solution).isEmpty();
        } else {
            assertThat(possibleSolutions).contains(solution);
        }
    }

}
