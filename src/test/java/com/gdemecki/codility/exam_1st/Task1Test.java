package com.gdemecki.codility.exam_1st;

import static org.fest.assertions.api.Assertions.assertThat;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Task1Test {

    private final Task1 instance = new Task1();

    @DataProvider
    Object[][] validTestCases() {
        return new Object[][] {
                { 14, 45 },
                { 10, 27 },
                { 1, 1 },
                { 2, 2 },
                { 2000, -1 }
        };
    }

    @Test(dataProvider = "validTestCases")
    public void shouldDivisiblesBeCalculatedCorrectly(int A, int expectedOutput) {
        assertThat(instance.solution(A))
                .isEqualTo(expectedOutput);
    }
    
}
