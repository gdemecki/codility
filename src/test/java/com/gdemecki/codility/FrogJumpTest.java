package com.gdemecki.codility;

import static org.fest.assertions.api.Assertions.assertThat;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class FrogJumpTest {

    FrogJump instance = new FrogJump();

    @DataProvider
    Object[][] validTestCases() {
        return new Object[][] {
                { 10, 85, 30, 3 },
                { 5, 5, 20, 0 },
                { 0, 1, 5, 1 },
                { 0, 10, 7, 2 },
                { 0, 10, 5, 2 },
                { 0, 11, 5, 3 },
                { 0, 0, 5, 0 },
                { 1, 1_000_000_000, 1_000, 1_000_000 },
        };
    }

    @Test(dataProvider = "validTestCases")
    public void shouldWork(int X, int Y, int D, int expectedOutput) {
        assertThat(instance.solution(X, Y, D)).isEqualTo(expectedOutput);
    }

}
