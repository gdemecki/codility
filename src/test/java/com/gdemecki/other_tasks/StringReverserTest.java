package com.gdemecki.other_tasks;

import static org.fest.assertions.api.Assertions.assertThat;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;

import com.gdemecki.other_tasks.StringReverser;

public class StringReverserTest {

    StringReverser instance = new StringReverser();

    @DataProvider
    public Object[][] validTestCases() {
        return new Object[][] {
                { "abcdefg", "gfedcba" },
                { "abc", "cba" },
                { "12345", "54321" },
                { "123456", "654321" }
        };
    }

    @Test(dataProvider = "validTestCases")
    public void reverse(String input, String expectedOutput) {
        assertThat(instance.reverse(input.toCharArray()))
                .isEqualTo(expectedOutput.toCharArray());
    }
}
