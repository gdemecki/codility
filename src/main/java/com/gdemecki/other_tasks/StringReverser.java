package com.gdemecki.other_tasks;

/**
 * Reverse string in place (without using {@code new StringBuilder(input).reverse()}).
 * 
 * @author Grzegorz Demecki
 * @since Jul 31, 2015
 */
public class StringReverser {

    public char[] reverse(char[] input) {
        int start = 0;
        int end = input.length - 1;
        while (start < end) {
            swap(input, start++, end--);
        }
        return input;
    }

    public char[] reverse_2nd(char[] input) {
        for (int i = 0; i < input.length / 2; i++) {
            swap(input, i, input.length - 1 - i);
        }
        return input;
    }

    private static void swap(char[] input, int i, int j) {
        char a = input[j];
        input[j] = input[i];
        input[i] = a;
    }

}
