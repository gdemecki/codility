package com.gdemecki.codility;

/**
 * Compute the height of a binary tree.
 * 
 * This task took me 27 minutes.
 * 
 * @author Grzegorz Demecki
 * @since Jul 31, 2015
 */
public class TreeHeight {

    int maxHeight = 0;

    public int solution(Tree T) {
        final Tree root = T;
        this.maxHeight = 0;

        calcHeight(root, 0);

        return maxHeight;
    }

    private void calcHeight(Tree root, int height) {
        if (root == null) {
            return;
        }

        if (height > maxHeight) {
            maxHeight = height;
        }

        calcHeight(root.l, height + 1);
        calcHeight(root.r, height + 1);
    }
}

class Tree {
    public int x;
    public Tree l;
    public Tree r;

    public Tree(int x) {
        this.x = x;
    }

    @Override
    public String toString() {
        return "root: " + Integer.toString(x);
    }
}