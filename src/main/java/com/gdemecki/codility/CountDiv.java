package com.gdemecki.codility;

/**
 * Compute number of integers divisible by k in range [a..b].
 * Difficulty: easy/ medium.
 * 
 * <p>
 * Task is available <a href=https://codility.com/demo/take-sample-test/count_div/>here</a>.
 * 
 * <h3>Full content of the task.</h3>
 * 
 * <pre>
 * Write a function:
 * 
 *     class Solution { public int solution(int A, int B, int K); }
 * 
 * that, given three integers A, B and K, returns the number of integers within the range [A..B] that are divisible by K, i.e.:
 * 
 *     { i : A ≤ i ≤ B, i mod K = 0 }
 * 
 * For example, for A = 6, B = 11 and K = 2, your function should return 3, because there are three numbers divisible by 2 within the range [6..11], namely 6, 8 and 10.
 * 
 * Assume that:
 * 
 *         A and B are integers within the range [0..2,000,000,000];
 *         K is an integer within the range [1..2,000,000,000];
 *         A ≤ B.
 * 
 * Complexity:
 * 
 *         expected worst-case time complexity is O(1);
 *         expected worst-case space complexity is O(1).
 * </pre>
 * 
 * This task took me 1.5h but still is not 100% correct. <br>
 * Finally solved task after additional 0.5h.
 * 
 * @author Grzegorz Demecki
 * @since 30 lip 2015
 */
public class CountDiv {

    public int solution(int A, int B, int K) {
        return correctSolution(A, B, K);
    }

    int correctSolution(int A, int B, int K) {
        int x = (int) Math.ceil(A / (double) K);
        int start = x * K;

        int range  = B - start;
        int div = (int) Math.ceil(range / (double) K);

        // corner case
        if (isDivisable(B, K)) {
            div++;
        }

        return div;
    }

    int optimalSolution_ButNotCorrect(int A, int B, int K) {
        final int diff = B - A;
        int div = (int) Math.floor(diff / (double) K);

        // corner case
        if (isDivisable(A, K) || isDivisable(B, K)) {
            div++;
        }

        return div;
    }

    int naiveSolution(int A, int B, int K) {
        int solution = 0;

        for (int i = A; i <= B; i++) {
            if (i % K == 0) {
                solution++;
            }
        }

        return solution;
    }

    boolean isDivisable(int x, int y) {
        return x % y == 0;
    }

}
