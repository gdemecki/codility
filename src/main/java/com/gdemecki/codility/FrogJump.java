package com.gdemecki.codility;

/**
 * Compute number of integers divisible by k in range [a..b].
 * Difficulty: easy.
 * 
 * <p>
 * Task is available <a
 * href=https://codility.com/demo/take-sample-test/frog_jmp/>here</a>.
 * 
 * <h3>Full content of the task.</h3>
 * 
 * <pre>
 * Write a function:
 * 
 *     int solution(int X, int Y, int D);
 * 
 * that, given three integers X, Y and D, returns the minimal number of jumps from position X to a position equal to or greater than Y.
 * 
 * For example, given:
 * 
 *   X = 10
 *   Y = 85
 *   D = 30
 * 
 * the function should return 3, because the frog will be positioned as follows:
 * 
 *         after the first jump, at position 10 + 30 = 40
 *         after the second jump, at position 10 + 30 + 30 = 70
 *         after the third jump, at position 10 + 30 + 30 + 30 = 100
 * 
 * Assume that:
 * 
 *         X, Y and D are integers within the range [1..1,000,000,000];
 *         X ≤ Y.
 * 
 * Complexity:
 * 
 *         expected worst-case time complexity is O(1);
 *         expected worst-case space complexity is O(1).
 * </pre>
 * 
 * This task took me 5 minutes. <br>
 * 
 * @author Grzegorz Demecki
 * @since 13 sie 2015
 */
public class FrogJump {

    public int solution(int X, int Y, int D) {
        int diff = Y - X;

        return (int) Math.ceil(diff / (double) D);
    }

}
