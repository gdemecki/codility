
# My results

## Total time

150 min 

## Aggregated total score

46%

## Detailed results

| Task no. | Total task score | Correctness | Performance |   |
|----------|------------------|-------------|-------------|---|
| Task 1   | 37%              | 50%         | 25%         |   |
| Task 2   | 72%              | 100%        | 25%         |   |
| Task 3   | 31%              | 62%         | 0%          |   |


IMHO this attempt went considerably better - although total score is slightly lower. I ruined this attempt by coming up with a wrong algorithm for `Task 1`.
