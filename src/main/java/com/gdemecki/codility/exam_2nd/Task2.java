package com.gdemecki.codility.exam_2nd;

public class Task2 {

    public int solution(int[] A) {
        return brutalForce(A);
    }

    public int brutalForce(int[] A) {
        final int N = A.length;
        if (N == 0) {
            return -1;
        } else if (N == 1) {
            return 0;
        }

        for (int i = 0; i < N; i++) {
            if (isMagnitude(A, i)) {
                return i;
            }
        }

        return -1;
    }

    boolean isMagnitude(int[] A, int Q) {
        final int x = A[Q];

        for (int i = 0; i < Q; i++) {
            if (A[i] > x) {
                return false;
            }
        }
        for (int i = Q + 1; i < A.length; i++) {
            if (A[i] < x) {
                return false;
            }
        }

        return true;
    }

}
