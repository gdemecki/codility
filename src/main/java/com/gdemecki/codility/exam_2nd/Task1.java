package com.gdemecki.codility.exam_2nd;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This is my final solution. I believe it is a 100% correct algorithm and has optimal
 * performance.
 *
 * <p>
 * Thoughts about algorithm:
 * <ul>
 * <li>it has to traverse whole input array
 * <li>it has to know the when the target condition is encountered (set of all different
 * numbers from the input) and remember the shortest range
 * <li>for efficiency: it has to remember the last occurrence of each number
 * </ul>
 * 
 * <p>
 * <b>Originally this task has ruined my 2nd exam final result</b>, because my solution
 * has not covered all possible use cases. Initially I had created 8 input data
 * test-cases, but they were not heterogeneous enough. And this resulted in only
 * <b>37%</b> score.
 *
 * <p>
 * This task took me 4h.
 *
 * @author Grzegorz Demecki
 * @since 16 sie 2015
 */
public class Task1 {

    public int solution(int[] A) {
        final int N = A.length;
        final int numOfUniqueLocations = countUniqueLocations(A);

        // small optimization
        if (numOfUniqueLocations < 3)
            return numOfUniqueLocations;

        int shortestRange = N; // initial value
        Map<Integer, Integer> lastOccurOf = new HashMap<>(); // index of last occurrence
        boolean occurArray[] = new boolean[N]; // twin of the input array; 'false' means
                                               // 'not needed'; 'true' means 'needed'

        // traverse array with two pointers: `begin` and `end`
        int begin = 0;
        for (int end = 0; end < N; end++) {
            final int curr = A[end];
            occurArray[end] = true;
            Integer lastOccurIdx = lastOccurOf.put(curr, end);

            if (metTargetCondition(lastOccurOf.size(), numOfUniqueLocations)) {
                int rangeIncl = end - begin + 1;
                if (rangeIncl < shortestRange)
                    shortestRange = rangeIncl;
            }

            if (lastOccurIdx != null) {
                occurArray[lastOccurIdx] = false;
                if (lastOccurIdx == begin) {
                    begin = forwardBeginPointer(occurArray, begin);
                }
            }
        }

        return shortestRange;
    }

    private boolean metTargetCondition(int encounteredLocs, int totalLocs) {
        return encounteredLocs == totalLocs;
    }

    private int countUniqueLocations(int[] array) {
        final Set<Integer> locations = new HashSet<>(array.length);
        for (int i = 0; i < array.length; i++)
            locations.add(array[i]);

        return locations.size();
    }

    private int forwardBeginPointer(boolean[] occurArray, int begin) {
        while (occurArray[begin++] == false);

        return begin - 1;
    }
}
