package com.gdemecki.codility.exam_2nd;

import java.util.HashSet;
import java.util.Set;

public class Task3Triangle {

    public int solution(int[] A) {
        final int N = A.length;
        if (N < 3) {
            return -1;
        }

        // hack to protect against weird input
        final Set<Integer> allElements = new HashSet<>();
        for (int i = 0; i < N; i++) {
            allElements.add(A[i]);
        }
        if (allElements.size() < 3) {
            return -1;
        }

        // brutal force ... no time to come up with other solution
        Integer minPerimeter = null;
        for (int i = 0; i < N - 2; i++) {
            for (int j = i + 1; j < N -1; j++) {
                for (int k = j + 1; k < N; k++) {
                    if (isTrianglePossible(A[i], A[j], A[k])) {
                        final int perimeter = A[i] + A[j] + A[k];

                        if (minPerimeter == null) {
                            minPerimeter = perimeter;
                        } else {
                            if (perimeter < minPerimeter) {
                                minPerimeter = perimeter;
                            }
                        }
                    }
                }
            }
        }

        return minPerimeter == null ? -1 : minPerimeter;
    }

    boolean isTrianglePossible(int a, int b, int c) {
        return a < b + c && b < a + c && c < a + b;
    }

}
