
# My results

## Total time

160 min 

## Aggregated total score

48%

## Detailed results

| Task no. | Total task score | Correctness | Performance |   |
|----------|------------------|-------------|-------------|---|
| Task 1   | 100%             | 100%        | N/A         |   |
| Task 2   | 33%              | 60%         | 0%          |   |
| Task 3   | 12%              | 20%         | 0%          |   |