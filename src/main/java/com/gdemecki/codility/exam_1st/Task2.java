package com.gdemecki.codility.exam_1st;

/**
 * This is my final solution. I believe it is a 100% correct algorithm and has optimal
 * performance.
 * 
 * <pre>
 * Given zero-indexed input array A of integers within range [-1.000.000.000, 1.000.000.000].
 * Calculate the biggest possible sum distance within this array.
 * Sum distance is: {@code A[p] + A[q] + (q - p)}.
 * 0 <= p <= q < N.
 * 
 * Expected performance is O(n).
 * </pre>
 * 
 * <p>
 * <b>Originally this task has ruined my 1st exam final result</b>, because my solution
 * has not covered all possible use cases. Initially I had created 5 input data
 * test-cases, but they were not heterogeneous enough. And this resulted in only
 * <b>33%</b> score (even though performance was optimal).
 * 
 * <p>
 * This task took me 60 minutes + 60 min during second attempt.
 * 
 * <p>
 * Original score: Correctness 60%; Performance: 0%.<br>
 * Now score: Correctness 100%; Performance: 100%.
 * 
 * @author Grzegorz Demecki
 * @since Aug 10, 2015
 */
public class Task2 {

    public int solution(int[] A) {
        return optimalSolution(A);
    }

    int optimalSolution(int[] A) {
        int biggestSumDistance = 2 * A[0];

        for (int p = 0, q = 0; q < A.length; q++) {
            int diff = Math.abs(q - p);

            if (A[q] - diff > A[p]) {
                p = q; // forward begin poiner
                diff = 0;
            }

            int sumDistance = A[p] + A[q] + diff;
            if (sumDistance > biggestSumDistance) {
                biggestSumDistance = sumDistance;
            }
        }

        return biggestSumDistance;
    }

    int brutalForce(int[] A) {
        int biggestSumDistance = 2 * A[0];

        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A.length; j++) {
                int sumDistance = A[i] + A[j] + (j - i);
                if (sumDistance > biggestSumDistance) {
                    biggestSumDistance = sumDistance;
                }
            }
        }

        return biggestSumDistance;
    }
}