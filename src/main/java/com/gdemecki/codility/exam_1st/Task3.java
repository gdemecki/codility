package com.gdemecki.codility.exam_1st;

/**
 * Calculate maximal odd decomposition.
 * 
 * <pre>
 * Expected performance is {@code O(sqrt(N))}.
 * Expected memory complexity is {@code O(sqrt(N))}.
 * </pre>
 * 
 * This task took me 50 minutes. But I've failed to finish it...
 * Result: Correctness 20%; Performance: 0%.
 * 
 * @author Grzegorz Demecki
 * @since Aug 10, 2015
 */
public class Task3 {

    public int[] solution(int N) {
        return maximalOddDecomposition(N);
    }

    private int[] maximalOddDecomposition(int N) {
        // corner cases
        if (N == 1) {
            return new int[] { 1 };
        } else if (N == 2) {
            int[] emptyArray = new int[0];
            return emptyArray;
        }

        int sqrt = (int) Math.ceil(Math.sqrt(N));
        final int[] decomposition = new int[sqrt];
        int depth = 0; // = decomposition length

        calcDecomposition(decomposition, depth, N);

        // TODO [gdemecki]: return final result
        return new int[] { N };
    }

    private void calcDecomposition(int[] decomposition, int depth, int N) {
        for (int i = 1; i <= N; i += 2) {
            decomposition[depth] = i;

            // calcDecomposition(decomposition, depth + 1, N);
        }
    }

}