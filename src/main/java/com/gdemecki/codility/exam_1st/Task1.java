package com.gdemecki.codility.exam_1st;

import java.math.BigInteger;

/**
 * Calculate the sum of values of the digits in a decimal representation of N! (factorial
 * of N).
 * 
 * For example, given N = 14, the function should return 45, because N! = 87178291200 and
 * the sum of the values of its digits equals:
 * 
 * <code>
 * 8 + 7 + 1 + 7 + 8 + 2 + 9 + 1 + 2 + 0 + 0 = 45.
 * </code>
 * 
 * This task took me 40 minutes.
 * Result: Correctness 100%; Performance: N/A.
 * 
 * @author Grzegorz Demecki
 * @since Aug 10, 2015
 */
public class Task1 {

    public int solution(int N) {

        final BigInteger factorial = factorial(N);
        final String digits = factorial.toString();

        int sum = 0;
        for (int i = 0; i < digits.length(); i++) {
            int digit = digits.charAt(i) - 48;
            sum += digit;
        }

        return sum;
    }

    BigInteger factorial(int x) {
        if (x <= 1) {
            return BigInteger.valueOf(1L);
        }
        BigInteger partial = factorial(x - 1);
        return partial.multiply(BigInteger.valueOf(x));
    }
}