# My preparation before codility exam

Project consists of some tasks, written solely for learning purposes as a preparation before exam.

## About Codility platform

Unfortunately tasks are mainly about algorithms. And don't belong to easy ones...

Whole exam consists of 3 tasks. They have to be solved wihout interruption in 2.5 hour.


## Lessons learnt

* Be careful about corner cases
* Prime numbers are special cases
* Remember about rounding errors when dividing
* Remember about automatic casting to integer a result of two integers' division
* Remember that casting is always before operand, e.g.: `Math.floor(diff / (double) K)`
* Remember about Math.floor (round down) and its opposite Math.ceil (round up)
* Google can be useful during exam (to search for solutions of similar tasks - not to trick, but for better insight of the optimal solution's direction).


## Final calculation of the test score

* Correctness 100% and Performance   0%  = 59 %
* Correctness  62% and Performance   0%  = 31 %
* Correctness  60% and Performance   0%  = 33 %
* Correctness  50% and Performance  25%  = 37 %

So weight of both above factors differs among tasks.

Tip: although performance is also considered - but Correctness is still far more important.

# How to make it?

## Before

* Prepare IDE (**do not** code in a web browser: none autocompletion, also single Java compilation takes 8-10 seconds!)
* Prepare sample project with single test case
* Prepare DataProvider (when using TestNG)
* Install Infinitest Eclipse plugin (**abandoned**: I don't find it useful/ convenient)

## When started

* Prepare **three** simplest **but correct** input test data (using piece of paper and pencil)
* Prepare **two** complex input test data
* Plus two corner cases (for edges of the input interval)
* Plus two for prime numbers
* Plus generate one/ two test data for big numbers

Above steps can take ~25 minutes but it really pays off.

## Next

* Design algorithm on paper.
  Try to comply with the **commanded complexity**. If failed then fall back to a "brutal force" approach. Because inefficient algorithm is still far better than nothing.


## Next

* When task is solved, make code clean. (not needed, as the **score** is usually the only thing taken into account)

Submit solution